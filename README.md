# README #
[Windows Application]
This repository houses a long term test targeting the <oscillator name>. 
Run it via `python test_osc.py`

### Repository Info ###

* V1
* This application will run periodic tests/tasks targetting the <oscillator name>. 
The test will turn on the oscillator (via a <power device name>) and let it run for 
<on time> before turning it off for <off time>. 
This will cycle <total cycles> times and log relevant oscillator data.

### Setup ###

If you do not have the git command line utility already, download it here <https://git-scm.com/download/win>.
Similarly if you do not have python3.6 or a more recent version installed, 
install it here <https://www.python.org/downloads/>.

1. Navigate to <https://bitbucket.org/JaiMehraOrolia/randys_oscillator_test> and press **clone**, 
select **HTTPS** in the dropdown instead of **SSH** and copy the command.
Open the command prompt and navigate to the directory you would like to store this application.
Run the command you copied. This will download the repository on to your machine (You **MUST** have orolia bitbucket access)
2. In the same command prompt run `python -m pip install -r requirements.txt`. 
This will install all of the python dependencies for you which could take a minute.
3. [**IMPORTANT**] In order to write to the USB devices, NI-Visa drivers must be installed for Windows 
<https://www.ni.com/en-us/support/downloads/drivers/download.ni-visa.html#346210> 
