import pyvisa as visa
from contextlib import contextmanager


class USBDevice:

    def __init__(self, com_port=None):
        self.com_port = com_port.lstrip('COM')
        self.rm = visa.ResourceManager()

    def __get_device(self):
        usb_identifier = f'ASRL{self.com_port}::INSTR'
        dev = self.rm.open_resource(usb_identifier, resource_pyclass=visa.resources.SerialInstrument)
        return dev

    @contextmanager
    def __connection_context(self, *args, **kwargs):
        connection = self.__get_device()
        try:
            yield connection
        finally:
            connection.close()

    def _list_resources(self):
        return self.rm.list_resources()

    def write(self, cmd):
        with self.__connection_context() as conn:
            bytes_written = conn.write(cmd)
        return bytes_written

    def read(self, cmd):
        with self.__connection_context() as conn:
            output = conn.read(cmd)
        return output

    def query(self, cmd, delay=None):
        with self.__connection_context() as conn:
            output = conn.query(cmd, delay=delay)
        return output


if __name__ == "__main__":
    usb = USBDevice("COM13")
