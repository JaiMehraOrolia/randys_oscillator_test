import socket
import select
from contextlib import contextmanager


class TCPDevice():
    def __init__(self, host, port=23, conn_timeout=10):
        self.host = host
        self.port = port
        self.conn_timeout = conn_timeout
        self.s = None

    @contextmanager
    def __connection_context(self, *args, **kwargs):
        self.connect()
        self.read()  # Clear welcome message if any
        try:
            yield
        finally:
            self.close()

    def write(self, cmd):
        if self.s:
            if type(cmd) is str:
                cmd = cmd.encode()
            if not cmd.endswith(b'\r\n'):
                cmd += b'\r\n'
                # Send message & wait for response
            self.s.send(cmd)
            return None

        with self.__connection_context():
            # Clean & make sure message is formatted correctly
            if type(cmd) is str:
                cmd = cmd.encode()
            if not cmd.endswith(b'\r\n'):
                cmd += b'\r\n'
            # Send message & wait for response
            self.s.send(cmd)
            return None

    def read(self, read_timeout=2):
        if self.s:
            reply = []
            while True:
                read_ready, _, _ = select.select([self.s], [], [], read_timeout)
                if read_ready:  # If there is info ready to read then read it
                    reply.append(self.s.recv(2048))
                    read_timeout = 0.1
                    continue
                break
            return b''.join(reply) if reply else b''

        with self.__connection_context():
            reply = []
            while True:
                read_ready, _, _ = select.select([self.s], [], [], read_timeout)
                if read_ready:  # If there is info ready to read then read it
                    reply.append(self.s.recv(2048))
                    read_timeout = 0.1
                    continue
                break
            return b''.join(reply) if reply else b''

    def query(self, cmd, read_timeout=2):
        with self.__connection_context():
            self.write(cmd=cmd)
            reply = self.read(read_timeout=read_timeout)
            return reply

    def connect(self):
        if not self.s:
            # Create the socket object
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.settimeout(self.conn_timeout)  # Connection timeout
            try:
                s.connect((self.host, self.port))  # Try to connect
            except ConnectionRefusedError:
                s.close()
                return None
            # If connected then prevent blocking on reads
            s.setblocking(0)
            self.s = s

    def close(self):
        if self.s:
            self.s.close()
            self.s = None

if __name__ == "__main__":
    host, port = '192.168.1.100', 23
    dev = TCPDevice(host=host, port=port)
    dev.query('$A3 1 0')

