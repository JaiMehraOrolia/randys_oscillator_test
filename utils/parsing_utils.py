import yaml


def parse_cfg(cfg_file_path):
    with open(cfg_file_path, 'r') as file:
        cfg = yaml.load(file, Loader=yaml.BaseLoader)

    cfg['power'] = flatten_cnx_args(cfg['power'])
    for idx, osc in enumerate(cfg['oscillators']):
        cfg['oscillators'][idx] = flatten_cnx_args(cfg['oscillators'][idx])

    return cfg

def flatten_cnx_args(cnx_args):
    args = cnx_args['connection_args']
    if 'port' in args:
        try:
            args['port'] = int(args['port'])
        except:
            pass
    return args


if __name__ == '__main__':
    cfg = parse_cfg('usb_config.yaml')
    print(cfg)
