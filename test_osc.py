from utils.devices import PowerSupply, Oscillator
from utils.parsing_utils import parse_cfg
from time import sleep, monotonic
from pathlib import Path
from datetime import datetime as dt
from collections import namedtuple
import logging
import csv


# TEST SPECIFIC GLOBAL VARIABLES
TEST_CYCLES = 3  # Number of test cycles
# ON_TIME = 40*60  # Time to keep oscillators on (seconds)
# OFF_TIME = 40*60  # Time to keep oscillators off (seconds)
ON_TIME = 10  # Time to keep oscillators on (seconds)
OFF_TIME = 10  # Time to keep oscillators off (seconds)


# FILE CONSTANTS
TEST_DIR = Path(__file__).parent.absolute()
LOG_DIR = TEST_DIR/"logs"
OSC_LOG_DIRS = None
LOG_HEADERS = ['Datetime', 'Cycle', 'Status'] + Oscillator.m_headers
OSC_LOG_INFO = namedtuple('OscInfo', LOG_HEADERS)
CFG_FILE_PATH = TEST_DIR / 'usb_config.yaml'
CFG_INFO = parse_cfg(CFG_FILE_PATH)


def setup_logger():
    _logger = logging.getLogger(__name__)
    _logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] %(message)s')
    handler.setFormatter(formatter)
    _logger.addHandler(handler)
    return _logger


# Just going to sneak the logger in here real quick
logger = setup_logger()


def setup_devices():
    ''' Initialize the Power Supply & Oscillator classes w/ their identifiers. Also assign log filepaths '''
    power = PowerSupply(**CFG_INFO['power'])
    oscillators = [Oscillator(**osc_port) for osc_port in CFG_INFO['oscillators']]
    # Quickly power on the oscillators to retrieve their serial numbers
    power.toggle(on=True)
    sleep(5)
    for osc in oscillators:
        osc.get_serial()
    power.toggle(on=False)
    # Create the log filepaths using the date-time & oscillator serial number in the log filename
    global OSC_LOG_DIRS
    dt_win_fmt = dt.now().isoformat().split(".")[0].replace(":","_")
    OSC_LOG_DIRS = {osc.serial: LOG_DIR / f'[{dt_win_fmt}]_{osc.serial}.csv' for osc in oscillators}

    return power, oscillators


def setup_log_files():
    ''' Create & initialize the log files for each oscillator '''
    LOG_DIR.mkdir(exist_ok=True)  # Create the test logs directory if it doesn't exist
    # Create a new log file for each oscillator (or clear an existing one)
    for log_file in OSC_LOG_DIRS.values():
        # Either init as CSV
        with open(log_file, 'w', newline='') as file:
            writer = csv.DictWriter(file, fieldnames=LOG_HEADERS)
            writer.writeheader()


def write_to_csv(file_dir, **kwargs):
    # Either write the contents as CSV format
    with open(file_dir, 'a', newline='') as file:
        writer = csv.DictWriter(file, fieldnames=LOG_HEADERS)
        writer.writerow(kwargs)


def main():
    # [SETUP]: Setup the test devices & their respective log files
    logger.info('[SETUP]: Retrieving oscillator serial numbers & creating log directory')
    power_supply, oscillators = setup_devices()
    setup_log_files()

    # [TEST]: Start the test cycles
    logger.info(f'[TEST]: Running oscillator test for {TEST_CYCLES} cycles')
    for cycle_num in range(TEST_CYCLES):
        # Step 1.) Toggle on the Power Supply & sleep
        power_supply.toggle(on=True)
        sleep(ON_TIME)

        # Step 2.) Retrieve each Oscillator's status & internal measurments
        osc_ok = []
        for osc in oscillators:
            try:
                status = osc.status()
                measurement = osc.measure()
                osc_ok.append(f'Osc {osc.serial}: OK')
            except Exception as err:
                status = osc.st_error
                measurement = osc.m_error
                osc_ok.append(f'Osc {osc.serial}: Error Reading')
            finally:
                log_info = OSC_LOG_INFO(Datetime=dt.now().isoformat(),
                                        Cycle=cycle_num,
                                        Status=status,
                                        **measurement)
                # Step 3.) Log the status & measurements in .csv format
                write_to_csv(OSC_LOG_DIRS[osc.serial], **log_info._asdict())

        # Step 4.) Toggle off the power supply & sleep (unless this is the last cycle)
        power_supply.toggle(on=False)
        if cycle_num < TEST_CYCLES - 1:
            sleep(OFF_TIME)
        logger.info(f'[TEST]: Test cycle {cycle_num} complete. Cycle status ({" | ".join(osc_ok)})')

    # [CLEANUP]: Power off the test setup
    logger.info('[CLEANUP]: Powering off test devices')
    power_supply.toggle(on=False)


if __name__ == "__main__":
    main()
