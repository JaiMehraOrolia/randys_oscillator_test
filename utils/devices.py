from utils.tcp_utils import TCPDevice
from utils.usb_utils import USBDevice


class PowerSupply:

    def __init__(self, **kwargs):
        self.host = kwargs.get('host', None)
        self.port = kwargs.get('port', None)
        if (not self.host and not self.port) or (not self.host and self.port):
            ValueError("Device must be passed either a host & port or just a port (in the case of USB)")

        if self.host and self.port:
            self.dev = TCPDevice(self.host, self.port)
        else:
            self.dev = USBDevice(self.port)

    def toggle(self, on=True):
        cmd = f"$A3 1 {int(on)}"
        reply = self.dev.query(cmd=cmd)
        return reply


class Oscillator:
    m_headers = ["DC_Frequency_Adjustment_Voltage", "Reserved_1", "Rb_Signal_Peak_Voltage",
                 "DC_Photocell_Voltage", "VCXO_Control_Voltage", "Rb_Lamp_Heating_Limiting_Current",
                 "Rb_Cell_Heating_Limiting_Current", "Reserved_2"]

    def __init__(self, **kwargs):
        self.host = kwargs.get('host', None)
        self.port = kwargs.get('port', None)
        self.dev = USBDevice(self.port)
        self.serial = None
        # Dummy Error messages for use in reporting
        self.st_error = '[ERROR] Error Querying Oscillator Status'
        self.m_error = dict(zip(
            self.m_headers,
            ['[ERROR] Error Querying Oscillator Measurements' for _ in range(len(self.m_headers))]
        ))

    def __parse_measurement(self, measurement):
        hex_list = measurement.split()
        return dict(zip(self.m_headers, hex_list))

    def measure(self):
        cmd = "M"
        measurements = self.dev.query(cmd=cmd)
        return self.__parse_measurement(measurements.rstrip())

    def status(self):
        cmd = "ST"
        reply = self.dev.query(cmd=cmd)
        return reply.rstrip()

    def get_serial(self):
        if self.serial is None:
            cmd = "SN"
            reply = self.dev.query(cmd=cmd)
            self.serial = reply.rstrip()

        return self.serial


if __name__ == '__main__':
    host, port = '192.168.1.100', 23
    pwr = PowerSupply(host=host, port=port)
